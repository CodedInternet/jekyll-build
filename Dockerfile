FROM alpine:latest

# Install in order of updates, starting with ruby
RUN apk add --no-cache --virtual .build-deps
RUN apk add --no-cache --upgrade ruby &&\
    gem install bundler --no-ri --no-rdoc
RUN apk add --no-cache --upgrade gcc musl-dev jpeg-dev zlib-dev libffi-dev cairo-dev pango-dev gdk-pixbuf-dev python3-dev
RUN apk add --no-cache --upgrade npm &&\
    npm install -g yarn
